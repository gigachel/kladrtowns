
if(typeof window.addEventListener=="function"){if(window.performance&&window.performance.timing){(function(){var orig_ws=null;if(typeof document.cookie=="string"){var m=document.cookie.match(/_ws=([0-9a-f]{90,})([; ]|$)/);if(m&&m.length>1){orig_ws=m[1];}}
window.addEventListener("load",function measure(event){window.removeEventListener("load",measure,false);setTimeout(function(){var timing=window.performance.timing;var evts=["unloadEventStart","unloadEventEnd","redirectStart","redirectEnd","domainLookupStart","domainLookupEnd","fetchStart","connectStart","connectEnd","secureConnectionStart","requestStart","responseStart","responseEnd","domLoading","domInteractive","domComplete","domContentLoadedEventStart","domContentLoadedEventEnd","loadEventStart","loadEventEnd"];var data={};var sum=0;for(var i=0;i<evts.length;i++){var checkPoint=timing[evts[i]];if(!isNaN(checkPoint)&&checkPoint>0){data[evts[i]]=checkPoint-timing.navigationStart;sum=sum+data[evts[i]];}}
if(0==sum){return;}
if(document.location&&document.location.href){data.url=document.location.href;}
if(orig_ws){data.orig_ws=orig_ws;}
if(typeof jQuery!="undefined"){jQuery.ajax({"type":"POST","dataType":"json","url":"/ws/trec","data":data,});}},0);});})();}};!(function(undefined){if(!String.prototype.trim){String.prototype.trim=function()
{return this.replace(/^\s+|\s+$/g,'');}}
if(!String.prototype.str_replace)
{String.prototype.str_replace=function(subj,repl){var ar=this.split(subj);return ar.join(repl);}}
if(!String.prototype.ucfirst){String.prototype.ucfirst=function()
{return this.charAt(0).toUpperCase()+this.slice(1);}}
if(!String.prototype.containHTML){String.prototype.containHTML=function(){var div=document.createElement('div');div.innerHTML=this;for(var i=0,len=div.childNodes.length;i<len;i++){if(div.childNodes[i].nodeType==1)return true;}
return false;}}
if(window.Node&&Node.prototype&&!Node.prototype.contains)
{Node.prototype.contains=function(arg){return!!(this.compareDocumentPosition(arg)&16)}}
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(searchElement,fromIndex){var i,pivot=(fromIndex)?fromIndex:0,length;if(!this){throw new TypeError();}
length=this.length;if(length===0||pivot>=length){return-1;}
if(pivot<0){pivot=length-Math.abs(pivot);}
for(i=pivot;i<length;i++){if(this[i]===searchElement){return i;}}
return-1;};}
try{if('localStorage'in window&&window['localStorage']!==null){Storage.prototype._setItem=Storage.prototype.setItem;Storage.prototype.setItem=function(key,value){try{this._setItem(key,value);}catch(error){return false;}};}}catch(e){}})();function $id(e)
{if(typeof e=="object")return e;if(document.getElementById)return(document.getElementById(e));else if(document.all)return document.all(e);else if(typeof(seekLayer)=="function")return seekLayer(document,e);return null;}
function hide(e){if($id(e))$id(e).style.display="none";}
function show(e,val,force){if(!val)val="";if(($id(e)&&$id(e).style.display=="none")||force)$id(e).style.display=val;}
function toggle(o){var o=$id(o);if(o){if(o.style.display==='none')show(o);else hide(o);}return false;}
function getParent(e){return((e.parentElement)?e.parentElement:((e.parentNode)?e.parentNode:null));}
function getText(id){var e=$id(id);if(e){if(e.innerText)return e.innerText;if(e.textContent)return e.textContent;return e.innerHTML;}}
function objectByString(o,s){var r={};r=_.clone(o);var a=s.split('.');while(a.length>1){var n=a.shift();if(n in r){r=r[n];}else{return;}}return{'obj':r,'key':a.shift()}};function objectHasProperty(o,p){var result=false;for(var i in o){if(i==p){return{obj:o,key:p};}else if(typeof o[i]=='object'){result=objectHasProperty(o[i],p);}else{result=o.hasOwnProperty(p);if(result!=false){return{obj:o,key:p};}}if(result!=false){return{obj:o[i],key:i};}}return false;}
function _cleanObject(obj){function cleanArray(arr){var ret_arr={};for(var index in arr){if(parseInt(index)>=0){ret_arr[parseInt(index)]=arr[index];}else{}}return ret_arr;}
if(obj instanceof Array){obj=cleanArray(obj);}
for(var i in obj){if(typeof obj[i]=='object'){obj[i]=_cleanObject(obj[i]);}else{if(!obj.hasOwnProperty(i)||typeof obj[i]=='function'){delete obj[i];}}}
return obj;};;function curBrowserIsSupportedByUs(bver){var p=new UAParser(),browser=p.getBrowser(),bver=typeof bver!=='undefined'?bver:{'IE':9}
if(browser.name=='Chrome'&&browser.major>=32){}
else if(browser.name=='IE'&&browser.major>=bver.IE){}
else if(browser.name=='Firefox'&&browser.major>=26){}
else if(browser.name=='Opera'&&browser.major==12){}
else if(browser.name=='Opera'&&browser.major>=18){}
else if(browser.name.indexOf('Safari')>-1&&browser.major>=7){}
else{return false;}
return true;};!(function(jQuery,origSlideDown){jQuery.fn.slideDown=function(speed,easing,callback){var self=this;var args=Array.prototype.slice.call(arguments);var origCallback;for(var i=0,len=args.length;i<len;i++){if(jQuery.isFunction(args[i])){origCallback=args[i];args[i]=function(){origCallback.call(this);self.css('overflow','');}}}
if(!origCallback){args.push(function(){self.css('overflow','');});}
return origSlideDown.apply(this,args);};})(jQuery,jQuery.fn.slideDown);;if(typeof(SJ)==='undefined')
window.SJ={doASAP:function(callback){$(document).ready(callback);}};if(typeof JSON!=='object'){JSON={};}
(function(){'use strict';function f(n){return n<10?'0'+n:n;}
if(typeof Date.prototype.toJSON!=='function'){Date.prototype.toJSON=function(key){return isFinite(this.valueOf())?this.getUTCFullYear()+'-'+
f(this.getUTCMonth()+1)+'-'+
f(this.getUTCDate())+'T'+
f(this.getUTCHours())+':'+
f(this.getUTCMinutes())+':'+
f(this.getUTCSeconds())+'Z':null;};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(key){return this.valueOf();};}
var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'},rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==='string'?c:'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);})+'"':'"'+string+'"';}
function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}
if(typeof rep==='function'){value=rep.call(holder,key,value);}
switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}
gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==='[object Array]'){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}
v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v;}
if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){if(typeof rep[i]==='string'){k=rep[i];v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}
v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}
if(typeof JSON.stringify!=='function'){JSON.stringify=function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}
rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSON.stringify');}
return str('',{'':value});};}
if(typeof JSON.parse!=='function'){JSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}
return reviver.call(holder,key,value);}
text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+
('0000'+a.charCodeAt(0).toString(16)).slice(-4);});}
if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}
throw new SyntaxError('JSON.parse');};}}());(function(window){var $=window.jQuery||window.angular.element;var rootElement=window.document.documentElement,$rootElement=$(rootElement);if(!rootElement.addEventListener)return;addGlobalEventListener('change',markValue);addValueChangeByJsListener(markValue);$.prototype.checkAndTriggerAutoFillEvent=jqCheckAndTriggerAutoFillEvent;addGlobalEventListener('blur',function(target){window.setTimeout(function(){findParentForm(target).find('input').checkAndTriggerAutoFillEvent();},20);});window.document.addEventListener('DOMContentLoaded',function(){forEach(document.getElementsByTagName('input'),markValue);window.setTimeout(function(){$rootElement.find('input').checkAndTriggerAutoFillEvent();},200);},false);return;function jqCheckAndTriggerAutoFillEvent(){var i,el;for(i=0;i<this.length;i++){el=this[i];if(!valueMarked(el)){markValue(el);triggerChangeEvent(el);}}}
function valueMarked(el){if(!("$$currentValue"in el)){el.$$currentValue=el.getAttribute('value');}
var val=el.value,$$currentValue=el.$$currentValue;if(!val&&!$$currentValue){return true;}
return val===$$currentValue;}
function markValue(el){el.$$currentValue=el.value;}
function addValueChangeByJsListener(listener){var jq=window.jQuery||window.angular.element,jqProto=jq.prototype;var _val=jqProto.val;jqProto.val=function(newValue){var res=_val.apply(this,arguments);if(arguments.length>0){forEach(this,function(el){listener(el,newValue);});}
return res;};}
function addGlobalEventListener(eventName,listener){rootElement.addEventListener(eventName,onEvent,true);function onEvent(event){var target=event.target;listener(target);}}
function findParentForm(el){while(el){if(el.nodeName==='FORM'){return $(el);}
el=el.parentNode;}
return $();}
function forEach(arr,listener){if(arr.forEach){return arr.forEach(listener);}
var i;for(i=0;i<arr.length;i++){listener(arr[i]);}}
function triggerChangeEvent(element){var doc=window.document;var event=doc.createEvent("HTMLEvents");event.initEvent("change",true,true);element.dispatchEvent(event);}})(window);'use strict';angular.module("ngLocale",[],["$provide",function($provide){var PLURAL_CATEGORY={ZERO:"zero",ONE:"one",TWO:"two",FEW:"few",MANY:"many",OTHER:"other"};$provide.value("$locale",{"DATETIME_FORMATS":{"AMPMS":["\u0434\u043e \u043f\u043e\u043b\u0443\u0434\u043d\u044f","\u043f\u043e\u0441\u043b\u0435 \u043f\u043e\u043b\u0443\u0434\u043d\u044f"],"DAY":["\u0432\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435","\u043f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a","\u0432\u0442\u043e\u0440\u043d\u0438\u043a","\u0441\u0440\u0435\u0434\u0430","\u0447\u0435\u0442\u0432\u0435\u0440\u0433","\u043f\u044f\u0442\u043d\u0438\u0446\u0430","\u0441\u0443\u0431\u0431\u043e\u0442\u0430"],"MONTH":["\u044f\u043d\u0432\u0430\u0440\u044f","\u0444\u0435\u0432\u0440\u0430\u043b\u044f","\u043c\u0430\u0440\u0442\u0430","\u0430\u043f\u0440\u0435\u043b\u044f","\u043c\u0430\u044f","\u0438\u044e\u043d\u044f","\u0438\u044e\u043b\u044f","\u0430\u0432\u0433\u0443\u0441\u0442\u0430","\u0441\u0435\u043d\u0442\u044f\u0431\u0440\u044f","\u043e\u043a\u0442\u044f\u0431\u0440\u044f","\u043d\u043e\u044f\u0431\u0440\u044f","\u0434\u0435\u043a\u0430\u0431\u0440\u044f"],"SHORTDAY":["\u0432\u0441","\u043f\u043d","\u0432\u0442","\u0441\u0440","\u0447\u0442","\u043f\u0442","\u0441\u0431"],"SHORTMONTH":["\u044f\u043d\u0432.","\u0444\u0435\u0432\u0440.","\u043c\u0430\u0440\u0442\u0430","\u0430\u043f\u0440.","\u043c\u0430\u044f","\u0438\u044e\u043d\u044f","\u0438\u044e\u043b\u044f","\u0430\u0432\u0433.","\u0441\u0435\u043d\u0442.","\u043e\u043a\u0442.","\u043d\u043e\u044f\u0431.","\u0434\u0435\u043a."],"fullDate":"EEEE, d MMMM y\u00a0'\u0433'.","longDate":"d MMMM y\u00a0'\u0433'.","medium":"dd.MM.yyyy H:mm:ss","mediumDate":"dd.MM.yyyy","mediumTime":"H:mm:ss","short":"dd.MM.yy H:mm","shortDate":"dd.MM.yy","shortTime":"H:mm"},"NUMBER_FORMATS":{"CURRENCY_SYM":"\u0440\u0443\u0431.","DECIMAL_SEP":",","GROUP_SEP":"\u00a0","PATTERNS":[{"gSize":3,"lgSize":3,"macFrac":0,"maxFrac":3,"minFrac":0,"minInt":1,"negPre":"-","negSuf":"","posPre":"","posSuf":""},{"gSize":3,"lgSize":3,"macFrac":0,"maxFrac":2,"minFrac":2,"minInt":1,"negPre":"-","negSuf":"\u00a0\u00a4","posPre":"","posSuf":"\u00a0\u00a4"}]},"id":"ru-ru","pluralCat":function(n){if(n%10==1&&n%100!=11){return PLURAL_CATEGORY.ONE;}if(n==(n|0)&&n%10>=2&&n%10<=4&&(n%100<12||n%100>14)){return PLURAL_CATEGORY.FEW;}if(n%10==0||n==(n|0)&&n%10>=5&&n%10<=9||n==(n|0)&&n%100>=11&&n%100<=14){return PLURAL_CATEGORY.MANY;}return PLURAL_CATEGORY.OTHER;}});}]);angular.module('angie').config(function($interpolateProvider,$sceProvider){$interpolateProvider.startSymbol('{%');$interpolateProvider.endSymbol('%}');var myNavigator=navigator.userAgent.toLowerCase();var IE_version=(myNavigator.indexOf('msie')!=-1)?parseInt(myNavigator.split('msie')[1]):false;if(IE_version&&IE_version<8)
$sceProvider.enabled(false);});angular.module('angie').config(['$httpProvider','$provide','$controllerProvider','$compileProvider',function($httpProvider,$provide,$controllerProvider,$compileProvider){$httpProvider.defaults.headers.common["X-Requested-With"]='XMLHttpRequest';$httpProvider.defaults.headers.post['Content-Type']='application/x-www-form-urlencoded;charset=utf-8';$httpProvider.defaults.transformRequest=function(data){if(data===undefined){return data;}
return $.param(data);};app.Providers={};app.Providers.$controllerProvider=$controllerProvider;app.Providers.$compileProvider=$compileProvider;app.Providers.$provide=$provide;app.Data=app.Data||{};}]);angular.module('angie').run(function($sniffer){$sniffer.history=$sniffer.history&&!($.browser&&$.browser.opera&&parseFloat($.browser.version)<=12.17);});!(function(doc,$){var cookie_name='resume_processing';var has_redirected_cookie='rpru';var CREATE_RESUME_FORM=/.*\/resume\/create.*/g;var EDIT_RESUME_FORM=/.*\/resume\/edit.*/g;var href=window.location.href;function getCookie(name){var matches=doc.cookie.match(new RegExp("(?:^|; )"+name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,'\\$1')+"=([^;]*)"));return matches?parseInt(decodeURIComponent(matches[1]),10):undefined;}
$(function(){if(getCookie(cookie_name)&&!getCookie(has_redirected_cookie)&&!(CREATE_RESUME_FORM.test(href)||EDIT_RESUME_FORM.test(href))){$.post('/resume_processing/redirect',{data:{step:3,redirect_url:href}});}});})(document,jQuery);