
function ajax_request(url,func_name,name_span,params)
{var http_request=false;if(window.XMLHttpRequest)
{http_request=new XMLHttpRequest();}
else if(window.ActiveXObject)
{try
{http_request=new ActiveXObject("Msxml2.XMLHTTP");}catch(e)
{try
{http_request=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){}}}
if(!http_request)
{return false;}
var mthd=(params)?'POST':'GET';http_request.onreadystatechange=function(){Content(http_request,func_name,name_span);};http_request.open(mthd,url,true);if(mthd=='POST')
{var headers={'Content-Type':'application/x-www-form-urlencoded','X-Requested-With':'XMLHttpRequest','Accept':'text/javascript, text/html, application/xml, text/xml, */*'};for(var name in headers)
http_request.setRequestHeader(name,headers[name]);}
http_request.send(params);}
function Content(http_request,func_name,name_span)
{if(http_request.readyState==4)
{if(http_request.status==200)
{if(typeof func_name=='function')
{func_name.call(http_request,http_request.responseText,name_span)}
else
{window[func_name](http_request.responseText,name_span);}}}}