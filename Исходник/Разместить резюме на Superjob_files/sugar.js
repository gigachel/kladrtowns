
(function(){var object=Object,array=Array,regexp=RegExp,date=Date,string=String,number=Number,math=Math,Undefined;var globalContext=typeof global!=='undefined'?global:this;var internalToString=object.prototype.toString;var internalHasOwnProperty=object.prototype.hasOwnProperty;var definePropertySupport=object.defineProperty&&object.defineProperties;var regexIsFunction=typeof regexp()==='function';var noKeysInStringObjects=!('0'in new string('a'));var typeChecks={};var matchedByValueReg=/^\[object Date|Array|String|Number|RegExp|Boolean|Arguments\]$/;var ClassNames='Boolean,Number,String,Array,Date,RegExp,Function'.split(',');var isBoolean=buildPrimitiveClassCheck('boolean',ClassNames[0]);var isNumber=buildPrimitiveClassCheck('number',ClassNames[1]);var isString=buildPrimitiveClassCheck('string',ClassNames[2]);var isArray=buildClassCheck(ClassNames[3]);var isDate=buildClassCheck(ClassNames[4]);var isRegExp=buildClassCheck(ClassNames[5]);var isFunction=buildClassCheck(ClassNames[6]);function isClass(obj,klass,cached){var k=cached||className(obj);return k==='[object '+klass+']';}
function buildClassCheck(klass){var fn=(klass==='Array'&&array.isArray)||function(obj,cached){return isClass(obj,klass,cached);};typeChecks[klass]=fn;return fn;}
function buildPrimitiveClassCheck(type,klass){var fn=function(obj){if(isObjectType(obj)){return isClass(obj,klass);}
return typeof obj===type;}
typeChecks[klass]=fn;return fn;}
function className(obj){return internalToString.call(obj);}
function initializeClasses(){initializeClass(object);iterateOverObject(ClassNames,function(i,name){initializeClass(globalContext[name]);});}
function initializeClass(klass){if(klass['SugarMethods'])return;defineProperty(klass,'SugarMethods',{});extend(klass,false,true,{'extend':function(methods,override,instance){extend(klass,instance!==false,override,methods);},'sugarRestore':function(){return batchMethodExecute(this,klass,arguments,function(target,name,m){defineProperty(target,name,m.method);});},'sugarRevert':function(){return batchMethodExecute(this,klass,arguments,function(target,name,m){if(m['existed']){defineProperty(target,name,m['original']);}else{delete target[name];}});}});}
function extend(klass,instance,override,methods){var extendee=instance?klass.prototype:klass;initializeClass(klass);iterateOverObject(methods,function(name,extendedFn){var nativeFn=extendee[name],existed=hasOwnProperty(extendee,name);if(isFunction(override)&&nativeFn){extendedFn=wrapNative(nativeFn,extendedFn,override);}
if(override!==false||!nativeFn){defineProperty(extendee,name,extendedFn);}
klass['SugarMethods'][name]={'method':extendedFn,'existed':existed,'original':nativeFn,'instance':instance};});}
function extendSimilar(klass,instance,override,set,fn){var methods={};set=isString(set)?set.split(','):set;set.forEach(function(name,i){fn(methods,name,i);});extend(klass,instance,override,methods);}
function batchMethodExecute(target,klass,args,fn){var all=args.length===0,methods=multiArgs(args),changed=false;iterateOverObject(klass['SugarMethods'],function(name,m){if(all||methods.indexOf(name)!==-1){changed=true;fn(m['instance']?target.prototype:target,name,m);}});return changed;}
function wrapNative(nativeFn,extendedFn,condition){return function(a){return condition.apply(this,arguments)?extendedFn.apply(this,arguments):nativeFn.apply(this,arguments);}}
function defineProperty(target,name,method){if(definePropertySupport){object.defineProperty(target,name,{'value':method,'configurable':true,'enumerable':false,'writable':true});}else{target[name]=method;}}
function multiArgs(args,fn,from){var result=[],i=from||0,len;for(len=args.length;i<len;i++){result.push(args[i]);if(fn)fn.call(args,args[i],i);}
return result;}
function flattenedArgs(args,fn,from){var arg=args[from||0];if(isArray(arg)){args=arg;from=0;}
return multiArgs(args,fn,from);}
function checkCallback(fn){if(!fn||!fn.call){throw new TypeError('Callback is not callable');}}
function isDefined(o){return o!==Undefined;}
function isUndefined(o){return o===Undefined;}
function hasProperty(obj,prop){return!isPrimitiveType(obj)&&prop in obj;}
function hasOwnProperty(obj,prop){return!!obj&&internalHasOwnProperty.call(obj,prop);}
function isObjectType(obj){return!!obj&&(typeof obj==='object'||(regexIsFunction&&isRegExp(obj)));}
function isPrimitiveType(obj){var type=typeof obj;return obj==null||type==='string'||type==='number'||type==='boolean';}
function isPlainObject(obj,klass){klass=klass||className(obj);try{if(obj&&obj.constructor&&!hasOwnProperty(obj,'constructor')&&!hasOwnProperty(obj.constructor.prototype,'isPrototypeOf')){return false;}}catch(e){return false;}
return!!obj&&klass==='[object Object]'&&'hasOwnProperty'in obj;}
function iterateOverObject(obj,fn){var key;for(key in obj){if(!hasOwnProperty(obj,key))continue;if(fn.call(obj,key,obj[key],obj)===false)break;}}
function simpleRepeat(n,fn){for(var i=0;i<n;i++){fn(i);}}
function simpleMerge(target,source){iterateOverObject(source,function(key){target[key]=source[key];});return target;}
function coercePrimitiveToObject(obj){if(isPrimitiveType(obj)){obj=object(obj);}
if(noKeysInStringObjects&&isString(obj)){forceStringCoercion(obj);}
return obj;}
function forceStringCoercion(obj){var i=0,chr;while(chr=obj.charAt(i)){obj[i++]=chr;}}
function Hash(obj){simpleMerge(this,coercePrimitiveToObject(obj));};Hash.prototype.constructor=object;var abs=math.abs;var pow=math.pow;var ceil=math.ceil;var floor=math.floor;var round=math.round;var min=math.min;var max=math.max;function withPrecision(val,precision,fn){var multiplier=pow(10,abs(precision||0));fn=fn||round;if(precision<0)multiplier=1/multiplier;return fn(val*multiplier)/multiplier;}
var HalfWidthZeroCode=0x30;var HalfWidthNineCode=0x39;var FullWidthZeroCode=0xff10;var FullWidthNineCode=0xff19;var HalfWidthPeriod='.';var FullWidthPeriod='．';var HalfWidthComma=',';var FullWidthDigits='';var NumberNormalizeMap={};var NumberNormalizeReg;function codeIsNumeral(code){return(code>=HalfWidthZeroCode&&code<=HalfWidthNineCode)||(code>=FullWidthZeroCode&&code<=FullWidthNineCode);}
function buildNumberHelpers(){var digit,i;for(i=0;i<=9;i++){digit=chr(i+FullWidthZeroCode);FullWidthDigits+=digit;NumberNormalizeMap[digit]=chr(i+HalfWidthZeroCode);}
NumberNormalizeMap[HalfWidthComma]='';NumberNormalizeMap[FullWidthPeriod]=HalfWidthPeriod;NumberNormalizeMap[HalfWidthPeriod]=HalfWidthPeriod;NumberNormalizeReg=regexp('['+FullWidthDigits+FullWidthPeriod+HalfWidthComma+HalfWidthPeriod+']','g');}
function chr(num){return string.fromCharCode(num);}
function getTrimmableCharacters(){return'\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u2028\u2029\u3000\uFEFF';}
function repeatString(str,num){var result='',str=str.toString();while(num>0){if(num&1){result+=str;}
if(num>>=1){str+=str;}}
return result;}
function stringToNumber(str,base){var sanitized,isDecimal;sanitized=str.replace(NumberNormalizeReg,function(chr){var replacement=NumberNormalizeMap[chr];if(replacement===HalfWidthPeriod){isDecimal=true;}
return replacement;});return isDecimal?parseFloat(sanitized):parseInt(sanitized,base||10);}
function padNumber(num,place,sign,base){var str=abs(num).toString(base||10);str=repeatString('0',place-str.replace(/\.\d+/,'').length)+str;if(sign||num<0){str=(num<0?'-':'+')+str;}
return str;}
function getOrdinalizedSuffix(num){if(num>=11&&num<=13){return'th';}else{switch(num%10){case 1:return'st';case 2:return'nd';case 3:return'rd';default:return'th';}}}
function getRegExpFlags(reg,add){var flags='';add=add||'';function checkFlag(prop,flag){if(prop||add.indexOf(flag)>-1){flags+=flag;}}
checkFlag(reg.multiline,'m');checkFlag(reg.ignoreCase,'i');checkFlag(reg.global,'g');checkFlag(reg.sticky,'y');return flags;}
function escapeRegExp(str){if(!isString(str))str=string(str);return str.replace(new RegExp("([\\/\'*+?|()\[\]{}.^$])","g"),'\\$1');}
function callDateGet(d,method){return d['get'+(d._utc?'UTC':'')+method]();}
function callDateSet(d,method,value){return d['set'+(d._utc&&method!='ISOWeek'?'UTC':'')+method](value);}
function stringify(thing,stack){var type=typeof thing,thingIsObject,thingIsArray,klass,value,arr,key,i,len;if(type==='string')return thing;klass=internalToString.call(thing)
thingIsObject=isPlainObject(thing,klass);thingIsArray=isArray(thing,klass);if(thing!=null&&thingIsObject||thingIsArray){if(!stack)stack=[];if(stack.length>1){i=stack.length;while(i--){if(stack[i]===thing){return'CYC';}}}
stack.push(thing);value=thing.valueOf()+string(thing.constructor);arr=thingIsArray?thing:object.keys(thing).sort();for(i=0,len=arr.length;i<len;i++){key=thingIsArray?i:arr[i];value+=key+stringify(thing[key],stack);}
stack.pop();}else if(1/thing===-Infinity){value='-0';}else{value=string(thing&&thing.valueOf?thing.valueOf():thing);}
return type+klass+value;}
function isEqual(a,b){if(a===b){return a!==0||1/a===1/b;}else if(objectIsMatchedByValue(a)&&objectIsMatchedByValue(b)){return stringify(a)===stringify(b);}
return false;}
function objectIsMatchedByValue(obj){var klass=className(obj);return matchedByValueReg.test(klass)||isPlainObject(obj,klass);}
function getEntriesForIndexes(obj,args,isString){var result,length=obj.length,argsLen=args.length,overshoot=args[argsLen-1]!==false,multiple=argsLen>(overshoot?1:2);if(!multiple){return entryAtIndex(obj,length,args[0],overshoot,isString);}
result=[];multiArgs(args,function(index){if(isBoolean(index))return false;result.push(entryAtIndex(obj,length,index,overshoot,isString));});return result;}
function entryAtIndex(obj,length,index,overshoot,isString){if(overshoot){index=index%length;if(index<0)index=length+index;}
return isString?obj.charAt(index):obj[index];}
function buildObjectInstanceMethods(set,target){extendSimilar(target,true,false,set,function(methods,name){methods[name+(name==='equal'?'s':'')]=function(){return object[name].apply(null,[this].concat(multiArgs(arguments)));}});}
initializeClasses();buildNumberHelpers();extend(object,false,false,{'keys':function(obj){var keys=[];if(!isObjectType(obj)&&!isRegExp(obj)&&!isFunction(obj)){throw new TypeError('Object required');}
iterateOverObject(obj,function(key,value){keys.push(key);});return keys;}});function arrayIndexOf(arr,search,fromIndex,increment){var length=arr.length,fromRight=increment==-1,start=fromRight?length-1:0,index=toIntegerWithDefault(fromIndex,start);if(index<0){index=length+index;}
if((!fromRight&&index<0)||(fromRight&&index>=length)){index=start;}
while((fromRight&&index>=0)||(!fromRight&&index<length)){if(arr[index]===search){return index;}
index+=increment;}
return-1;}
function arrayReduce(arr,fn,initialValue,fromRight){var length=arr.length,count=0,defined=isDefined(initialValue),result,index;checkCallback(fn);if(length==0&&!defined){throw new TypeError('Reduce called on empty array with no initial value');}else if(defined){result=initialValue;}else{result=arr[fromRight?length-1:count];count++;}
while(count<length){index=fromRight?length-count-1:count;if(index in arr){result=fn(result,arr[index],index,arr);}
count++;}
return result;}
function toIntegerWithDefault(i,d){if(isNaN(i)){return d;}else{return parseInt(i>>0);}}
function checkFirstArgumentExists(args){if(args.length===0){throw new TypeError('First argument must be defined');}}
extend(array,false,false,{'isArray':function(obj){return isArray(obj);}});extend(array,true,false,{'every':function(fn,scope){var length=this.length,index=0;checkFirstArgumentExists(arguments);while(index<length){if(index in this&&!fn.call(scope,this[index],index,this)){return false;}
index++;}
return true;},'some':function(fn,scope){var length=this.length,index=0;checkFirstArgumentExists(arguments);while(index<length){if(index in this&&fn.call(scope,this[index],index,this)){return true;}
index++;}
return false;},'map':function(fn,scope){var scope=arguments[1],length=this.length,index=0,result=new Array(length);checkFirstArgumentExists(arguments);while(index<length){if(index in this){result[index]=fn.call(scope,this[index],index,this);}
index++;}
return result;},'filter':function(fn){var scope=arguments[1];var length=this.length,index=0,result=[];checkFirstArgumentExists(arguments);while(index<length){if(index in this&&fn.call(scope,this[index],index,this)){result.push(this[index]);}
index++;}
return result;},'indexOf':function(search){var fromIndex=arguments[1];if(isString(this))return this.indexOf(search,fromIndex);return arrayIndexOf(this,search,fromIndex,1);},'lastIndexOf':function(search){var fromIndex=arguments[1];if(isString(this))return this.lastIndexOf(search,fromIndex);return arrayIndexOf(this,search,fromIndex,-1);},'forEach':function(fn){var length=this.length,index=0,scope=arguments[1];checkCallback(fn);while(index<length){if(index in this){fn.call(scope,this[index],index,this);}
index++;}},'reduce':function(fn){return arrayReduce(this,fn,arguments[1]);},'reduceRight':function(fn){return arrayReduce(this,fn,arguments[1],true);}});function buildTrim(){var support=getTrimmableCharacters().match(/^\s+$/);try{string.prototype.trim.call([1]);}catch(e){support=false;}
extend(string,true,!support,{'trim':function(){return this.toString().trimLeft().trimRight();},'trimLeft':function(){return this.replace(regexp('^['+getTrimmableCharacters()+']+'),'');},'trimRight':function(){return this.replace(regexp('['+getTrimmableCharacters()+']+$'),'');}});}
extend(Function,true,false,{'bind':function(scope){var fn=this,args=multiArgs(arguments,null,1),bound;if(!isFunction(this)){throw new TypeError('Function.prototype.bind called on a non-function');}
bound=function(){return fn.apply(fn.prototype&&this instanceof fn?this:scope,args.concat(multiArgs(arguments)));}
bound.prototype=this.prototype;return bound;}});extend(date,false,false,{'now':function(){return new date().getTime();}});function buildISOString(){var d=new date(date.UTC(1999,11,31)),target='1999-12-31T00:00:00.000Z';var support=d.toISOString&&d.toISOString()===target;extendSimilar(date,true,!support,'toISOString,toJSON',function(methods,name){methods[name]=function(){return padNumber(this.getUTCFullYear(),4)+'-'+
padNumber(this.getUTCMonth()+1,2)+'-'+
padNumber(this.getUTCDate(),2)+'T'+
padNumber(this.getUTCHours(),2)+':'+
padNumber(this.getUTCMinutes(),2)+':'+
padNumber(this.getUTCSeconds(),2)+'.'+
padNumber(this.getUTCMilliseconds(),3)+'Z';}});}
buildTrim();buildISOString();var English;var CurrentLocalization;var TimeFormat=['ampm','hour','minute','second','ampm','utc','offset_sign','offset_hours','offset_minutes','ampm']
var DecimalReg='(?:[,.]\\d+)?';var HoursReg='\\d{1,2}'+DecimalReg;var SixtyReg='[0-5]\\d'+DecimalReg;var RequiredTime='({t})?\\s*('+HoursReg+')(?:{h}('+SixtyReg+')?{m}(?::?('+SixtyReg+'){s})?\\s*(?:({t})|(Z)|(?:([+-])(\\d{2,2})(?::?(\\d{2,2}))?)?)?|\\s*({t}))';var KanjiDigits='〇一二三四五六七八九十百千万';var AsianDigitMap={};var AsianDigitReg;var DateArgumentUnits;var DateUnitsReversed;var CoreDateFormats=[];var CompiledOutputFormats={};var DateFormatTokens={'yyyy':function(d){return callDateGet(d,'FullYear');},'yy':function(d){return callDateGet(d,'FullYear')%100;},'ord':function(d){var date=callDateGet(d,'Date');return date+getOrdinalizedSuffix(date);},'tz':function(d){return d.getUTCOffset();},'isotz':function(d){return d.getUTCOffset(true);},'Z':function(d){return d.getUTCOffset();},'ZZ':function(d){return d.getUTCOffset().replace(/(\d{2})$/,':$1');}};var DateUnits=[{name:'year',method:'FullYear',ambiguous:true,multiplier:function(d){var adjust=d?(d.isLeapYear()?1:0):0.25;return(365+adjust)*24*60*60*1000;}},{name:'month',error:0.919,method:'Month',ambiguous:true,multiplier:function(d,ms){var days=30.4375,inMonth;if(d){inMonth=d.daysInMonth();if(ms<=inMonth.days()){days=inMonth;}}
return days*24*60*60*1000;}},{name:'week',method:'ISOWeek',multiplier:function(){return 7*24*60*60*1000;}},{name:'day',error:0.958,method:'Date',ambiguous:true,multiplier:function(){return 24*60*60*1000;}},{name:'hour',method:'Hours',multiplier:function(){return 60*60*1000;}},{name:'minute',method:'Minutes',multiplier:function(){return 60*1000;}},{name:'second',method:'Seconds',multiplier:function(){return 1000;}},{name:'millisecond',method:'Milliseconds',multiplier:function(){return 1;}}];var Localizations={};function Localization(l){simpleMerge(this,l);this.compiledFormats=CoreDateFormats.concat();}
Localization.prototype={getMonth:function(n){if(isNumber(n)){return n-1;}else{return this['months'].indexOf(n)%12;}},getWeekday:function(n){return this['weekdays'].indexOf(n)%7;},getNumber:function(n){var i;if(isNumber(n)){return n;}else if(n&&(i=this['numbers'].indexOf(n))!==-1){return(i+1)%10;}else{return 1;}},getNumericDate:function(n){var self=this;return n.replace(regexp(this['num'],'g'),function(d){var num=self.getNumber(d);return num||'';});},getUnitIndex:function(n){return this['units'].indexOf(n)%8;},getRelativeFormat:function(adu){return this.convertAdjustedToFormat(adu,adu[2]>0?'future':'past');},getDuration:function(ms){return this.convertAdjustedToFormat(getAdjustedUnit(ms),'duration');},hasVariant:function(code){code=code||this.code;return code==='en'||code==='en-US'?true:this['variant'];},matchAM:function(str){return str===this['ampm'][0];},matchPM:function(str){return str&&str===this['ampm'][1];},convertAdjustedToFormat:function(adu,mode){var sign,unit,mult,num=adu[0],u=adu[1],ms=adu[2],format=this[mode]||this['relative'];if(isFunction(format)){return format.call(this,num,u,ms,mode);}
mult=this['plural']&&num>1?1:0;unit=this['units'][mult*8+u]||this['units'][u];if(this['capitalizeUnit'])unit=simpleCapitalize(unit);sign=this['modifiers'].filter(function(m){return m.name=='sign'&&m.value==(ms>0?1:-1);})[0];return format.replace(/\{(.*?)\}/g,function(full,match){switch(match){case'num':return num;case'unit':return unit;case'sign':return sign.src;}});},getFormats:function(){return this.cachedFormat?[this.cachedFormat].concat(this.compiledFormats):this.compiledFormats;},addFormat:function(src,allowsTime,match,variant,iso){var to=match||[],loc=this,time,timeMarkers,lastIsNumeral;src=src.replace(/\s+/g,'[,. ]*');src=src.replace(/\{([^,]+?)\}/g,function(all,k){var value,arr,result,opt=k.match(/\?$/),nc=k.match(/^(\d+)\??$/),slice=k.match(/(\d)(?:-(\d))?/),key=k.replace(/[^a-z]+$/,'');if(nc){value=loc['tokens'][nc[1]];}else if(loc[key]){value=loc[key];}else if(loc[key+'s']){value=loc[key+'s'];if(slice){arr=[];value.forEach(function(m,i){var mod=i%(loc['units']?8:value.length);if(mod>=slice[1]&&mod<=(slice[2]||slice[1])){arr.push(m);}});value=arr;}
value=arrayToAlternates(value);}
if(nc){result='(?:'+value+')';}else{if(!match){to.push(key);}
result='('+value+')';}
if(opt){result+='?';}
return result;});if(allowsTime){time=prepareTime(RequiredTime,loc,iso);timeMarkers=['t','[\\s\\u3000]'].concat(loc['timeMarker']);lastIsNumeral=src.match(/\\d\{\d,\d\}\)+\??$/);addDateInputFormat(loc,'(?:'+time+')[,\\s\\u3000]+?'+src,TimeFormat.concat(to),variant);addDateInputFormat(loc,src+'(?:[,\\s]*(?:'+timeMarkers.join('|')+(lastIsNumeral?'+':'*')+')'+time+')?',to.concat(TimeFormat),variant);}else{addDateInputFormat(loc,src,to,variant);}}};function getLocalization(localeCode,fallback){var loc;if(!isString(localeCode))localeCode='';loc=Localizations[localeCode]||Localizations[localeCode.slice(0,2)];if(fallback===false&&!loc){throw new TypeError('Invalid locale.');}
return loc||CurrentLocalization;}
function setLocalization(localeCode,set){var loc,canAbbreviate;function initializeField(name){var val=loc[name];if(isString(val)){loc[name]=val.split(',');}else if(!val){loc[name]=[];}}
function eachAlternate(str,fn){str=str.split('+').map(function(split){return split.replace(/(.+):(.+)$/,function(full,base,suffixes){return suffixes.split('|').map(function(suffix){return base+suffix;}).join('|');});}).join('|');return str.split('|').forEach(fn);}
function setArray(name,abbreviate,multiple){var arr=[];loc[name].forEach(function(full,i){if(abbreviate){full+='+'+full.slice(0,3);}
eachAlternate(full,function(day,j){arr[j*multiple+i]=day.toLowerCase();});});loc[name]=arr;}
function getDigit(start,stop,allowNumbers){var str='\\d{'+start+','+stop+'}';if(allowNumbers)str+='|(?:'+arrayToAlternates(loc['numbers'])+')+';return str;}
function getNum(){var arr=['-?\\d+'].concat(loc['articles']);if(loc['numbers'])arr=arr.concat(loc['numbers']);return arrayToAlternates(arr);}
function setDefault(name,value){loc[name]=loc[name]||value;}
function setModifiers(){var arr=[];loc.modifiersByName={};loc['modifiers'].push({'name':'day','src':'yesterday','value':-1});loc['modifiers'].push({'name':'day','src':'today','value':0});loc['modifiers'].push({'name':'day','src':'tomorrow','value':1});loc['modifiers'].forEach(function(modifier){var name=modifier.name;eachAlternate(modifier.src,function(t){var locEntry=loc[name];loc.modifiersByName[t]=modifier;arr.push({name:name,src:t,value:modifier.value});loc[name]=locEntry?locEntry+'|'+t:t;});});loc['day']+='|'+arrayToAlternates(loc['weekdays']);loc['modifiers']=arr;}
loc=new Localization(set);initializeField('modifiers');'months,weekdays,units,numbers,articles,tokens,timeMarker,ampm,timeSuffixes,dateParse,timeParse'.split(',').forEach(initializeField);canAbbreviate=!loc['monthSuffix'];setArray('months',canAbbreviate,12);setArray('weekdays',canAbbreviate,7);setArray('units',false,8);setArray('numbers',false,10);setDefault('code',localeCode);setDefault('date',getDigit(1,2,loc['digitDate']));setDefault('year',"'\\d{2}|"+getDigit(4,4));setDefault('num',getNum());setModifiers();if(loc['monthSuffix']){loc['month']=getDigit(1,2);loc['months']='1,2,3,4,5,6,7,8,9,10,11,12'.split(',').map(function(n){return n+loc['monthSuffix'];});}
loc['full_month']=getDigit(1,2)+'|'+arrayToAlternates(loc['months']);if(loc['timeSuffixes'].length>0){loc.addFormat(prepareTime(RequiredTime,loc),false,TimeFormat)}
loc.addFormat('{day}',true);loc.addFormat('{month}'+(loc['monthSuffix']||''));loc.addFormat('{year}'+(loc['yearSuffix']||''));loc['timeParse'].forEach(function(src){loc.addFormat(src,true);});loc['dateParse'].forEach(function(src){loc.addFormat(src);});return Localizations[localeCode]=loc;}
function addDateInputFormat(locale,format,match,variant){locale.compiledFormats.unshift({variant:variant,locale:locale,reg:regexp('^'+format+'$','i'),to:match});}
function simpleCapitalize(str){return str.slice(0,1).toUpperCase()+str.slice(1);}
function arrayToAlternates(arr){return arr.filter(function(el){return!!el;}).join('|');}
function getNewDate(){var fn=date.SugarNewDate;return fn?fn():new date;}
function collectDateArguments(args,allowDuration){var obj;if(isObjectType(args[0])){return args;}else if(isNumber(args[0])&&!isNumber(args[1])){return[args[0]];}else if(isString(args[0])&&allowDuration){return[getDateParamsFromString(args[0]),args[1]];}
obj={};DateArgumentUnits.forEach(function(u,i){obj[u.name]=args[i];});return[obj];}
function getDateParamsFromString(str,num){var match,params={};match=str.match(/^(\d+)?\s?(\w+?)s?$/i);if(match){if(isUndefined(num)){num=parseInt(match[1])||1;}
params[match[2].toLowerCase()]=num;}
return params;}
function iterateOverDateUnits(fn,from,to){var i,unit;if(isUndefined(to))to=DateUnitsReversed.length;for(i=from||0;i<to;i++){unit=DateUnitsReversed[i];if(fn(unit.name,unit,i)===false){break;}}}
function getFormatMatch(match,arr){var obj={},value,num;arr.forEach(function(key,i){value=match[i+1];if(isUndefined(value)||value==='')return;if(key==='year'){obj.yearAsString=value.replace(/'/,'');}
num=parseFloat(value.replace(/'/,'').replace(/,/,'.'));obj[key]=!isNaN(num)?num:value.toLowerCase();});return obj;}
function cleanDateInput(str){str=str.trim().replace(/^just (?=now)|\.+$/i,'');return convertAsianDigits(str);}
function convertAsianDigits(str){return str.replace(AsianDigitReg,function(full,disallowed,match){var sum=0,place=1,lastWasHolder,lastHolder;if(disallowed)return full;match.split('').reverse().forEach(function(letter){var value=AsianDigitMap[letter],holder=value>9;if(holder){if(lastWasHolder)sum+=place;place*=value/(lastHolder||1);lastHolder=value;}else{if(lastWasHolder===false){place*=10;}
sum+=place*value;}
lastWasHolder=holder;});if(lastWasHolder)sum+=place;return sum;});}
function getExtendedDate(f,localeCode,prefer,forceUTC){var d,relative,baseLocalization,afterCallbacks,loc,set,unit,unitIndex,weekday,num,tmp;d=getNewDate();afterCallbacks=[];function afterDateSet(fn){afterCallbacks.push(fn);}
function fireCallbacks(){afterCallbacks.forEach(function(fn){fn.call();});}
function setWeekdayOfMonth(){var w=d.getWeekday();d.setWeekday((7*(set['num']-1))+(w>weekday?weekday+7:weekday));}
function setUnitEdge(){var modifier=loc.modifiersByName[set['edge']];iterateOverDateUnits(function(name){if(isDefined(set[name])){unit=name;return false;}},4);if(unit==='year')set.specificity='month';else if(unit==='month'||unit==='week')set.specificity='day';d[(modifier.value<0?'endOf':'beginningOf')+simpleCapitalize(unit)]();if(modifier.value===-2)d.reset();}
function separateAbsoluteUnits(){var params;iterateOverDateUnits(function(name,u,i){if(name==='day')name='date';if(isDefined(set[name])){if(i>=unitIndex){invalidateDate(d);return false;}
params=params||{};params[name]=set[name];delete set[name];}});if(params){afterDateSet(function(){d.set(params,true);});}}
d.utc(forceUTC);if(isDate(f)){d.utc(f.isUTC()).setTime(f.getTime());}else if(isNumber(f)){d.setTime(f);}else if(isObjectType(f)){d.set(f,true);set=f;}else if(isString(f)){baseLocalization=getLocalization(localeCode);f=cleanDateInput(f);if(baseLocalization){iterateOverObject(baseLocalization.getFormats(),function(i,dif){var match=f.match(dif.reg);if(match){loc=dif.locale;set=getFormatMatch(match,dif.to,loc);loc.cachedFormat=dif;if(set['utc']){d.utc();}
if(set.timestamp){set=set.timestamp;return false;}
if(dif.variant&&!isString(set['month'])&&(isString(set['date'])||baseLocalization.hasVariant(localeCode))){tmp=set['month'];set['month']=set['date'];set['date']=tmp;}
if(set['year']&&set.yearAsString.length===2){set['year']=getYearFromAbbreviation(set['year']);}
if(set['month']){set['month']=loc.getMonth(set['month']);if(set['shift']&&!set['unit'])set['unit']=loc['units'][7];}
if(set['weekday']&&set['date']){delete set['weekday'];}else if(set['weekday']){set['weekday']=loc.getWeekday(set['weekday']);if(set['shift']&&!set['unit'])set['unit']=loc['units'][5];}
if(set['day']&&(tmp=loc.modifiersByName[set['day']])){set['day']=tmp.value;d.reset();relative=true;}else if(set['day']&&(weekday=loc.getWeekday(set['day']))>-1){delete set['day'];if(set['num']&&set['month']){afterDateSet(setWeekdayOfMonth);set['day']=1;}else{set['weekday']=weekday;}}
if(set['date']&&!isNumber(set['date'])){set['date']=loc.getNumericDate(set['date']);}
if(loc.matchPM(set['ampm'])&&set['hour']<12){set['hour']+=12;}else if(loc.matchAM(set['ampm'])&&set['hour']===12){set['hour']=0;}
if('offset_hours'in set||'offset_minutes'in set){d.utc();set['offset_minutes']=set['offset_minutes']||0;set['offset_minutes']+=set['offset_hours']*60;if(set['offset_sign']==='-'){set['offset_minutes']*=-1;}
set['minute']-=set['offset_minutes'];}
if(set['unit']){relative=true;num=loc.getNumber(set['num']);unitIndex=loc.getUnitIndex(set['unit']);unit=English['units'][unitIndex];separateAbsoluteUnits();if(set['shift']){num*=(tmp=loc.modifiersByName[set['shift']])?tmp.value:0;}
if(set['sign']&&(tmp=loc.modifiersByName[set['sign']])){num*=tmp.value;}
if(isDefined(set['weekday'])){d.set({'weekday':set['weekday']},true);delete set['weekday'];}
set[unit]=(set[unit]||0)+num;}
if(set['edge']){afterDateSet(setUnitEdge);}
if(set['year_sign']==='-'){set['year']*=-1;}
iterateOverDateUnits(function(name,unit,i){var value=set[name],fraction=value%1;if(fraction){set[DateUnitsReversed[i-1].name]=round(fraction*(name==='second'?1000:60));set[name]=floor(value);}},1,4);return false;}});}
if(!set){if(f!=='now'){d=new date(f);}
if(forceUTC){d.addMinutes(-d.getTimezoneOffset());}}else if(relative){d.advance(set);}else{if(d._utc){d.reset();}
updateDate(d,set,true,false,prefer);}
fireCallbacks();d.utc(false);}
return{date:d,set:set}}
function getYearFromAbbreviation(year){return round(callDateGet(getNewDate(),'FullYear')/100)*100-round(year/100)*100+year;}
function getShortHour(d){var hours=callDateGet(d,'Hours');return hours===0?12:hours-(floor(hours/13)*12);}
function getWeekNumber(date){date=date.clone();var dow=callDateGet(date,'Day')||7;date.addDays(4-dow).reset();return 1+floor(date.daysSince(date.clone().beginningOfYear())/7);}
function getAdjustedUnit(ms){var next,ams=abs(ms),value=ams,unitIndex=0;iterateOverDateUnits(function(name,unit,i){next=floor(withPrecision(ams/unit.multiplier(),1));if(next>=1){value=next;unitIndex=i;}},1);return[value,unitIndex,ms];}
function getRelativeWithMonthFallback(date){var adu=getAdjustedUnit(date.millisecondsFromNow());if(allowMonthFallback(date,adu)){adu[0]=abs(date.monthsFromNow());adu[1]=6;}
return adu;}
function allowMonthFallback(date,adu){return adu[1]===6||(adu[1]===5&&adu[0]===4&&date.daysFromNow()>=getNewDate().daysInMonth());}
function createMeridianTokens(slice,caps){var fn=function(d,localeCode){var hours=callDateGet(d,'Hours');return getLocalization(localeCode)['ampm'][floor(hours/12)]||'';}
createFormatToken('t',fn,1);createFormatToken('tt',fn);createFormatToken('T',fn,1,1);createFormatToken('TT',fn,null,2);}
function createWeekdayTokens(slice,caps){var fn=function(d,localeCode){var dow=callDateGet(d,'Day');return getLocalization(localeCode)['weekdays'][dow];}
createFormatToken('dow',fn,3);createFormatToken('Dow',fn,3,1);createFormatToken('weekday',fn);createFormatToken('Weekday',fn,null,1);}
function createMonthTokens(slice,caps){createMonthToken('mon',0,3);createMonthToken('month',0);createMonthToken('month2',1);createMonthToken('month3',2);}
function createMonthToken(token,multiplier,slice){var fn=function(d,localeCode){var month=callDateGet(d,'Month');return getLocalization(localeCode)['months'][month+(multiplier*12)];};createFormatToken(token,fn,slice);createFormatToken(simpleCapitalize(token),fn,slice,1);}
function createFormatToken(t,fn,slice,caps){DateFormatTokens[t]=function(d,localeCode){var str=fn(d,localeCode);if(slice)str=str.slice(0,slice);if(caps)str=str.slice(0,caps).toUpperCase()+str.slice(caps);return str;}}
function createPaddedToken(t,fn,ms){DateFormatTokens[t]=fn;DateFormatTokens[t+t]=function(d,localeCode){return padNumber(fn(d,localeCode),2);};if(ms){DateFormatTokens[t+t+t]=function(d,localeCode){return padNumber(fn(d,localeCode),3);};DateFormatTokens[t+t+t+t]=function(d,localeCode){return padNumber(fn(d,localeCode),4);};}}
function buildCompiledOutputFormat(format){var match=format.match(/(\{\w+\})|[^{}]+/g);CompiledOutputFormats[format]=match.map(function(p){p.replace(/\{(\w+)\}/,function(full,token){p=DateFormatTokens[token]||token;return token;});return p;});}
function executeCompiledOutputFormat(date,format,localeCode){var compiledFormat,length,i,t,result='';compiledFormat=CompiledOutputFormats[format];for(i=0,length=compiledFormat.length;i<length;i++){t=compiledFormat[i];result+=isFunction(t)?t(date,localeCode):t;}
return result;}
function formatDate(date,format,relative,localeCode){var adu;if(!date.isValid()){return'Invalid Date';}else if(Date[format]){format=Date[format];}else if(isFunction(format)){adu=getRelativeWithMonthFallback(date);format=format.apply(date,adu.concat(getLocalization(localeCode)));}
if(!format&&relative){adu=adu||getRelativeWithMonthFallback(date);if(adu[1]===0){adu[1]=1;adu[0]=1;}
return getLocalization(localeCode).getRelativeFormat(adu);}
format=format||'long';if(format==='short'||format==='long'||format==='full'){format=getLocalization(localeCode)[format];}
if(!CompiledOutputFormats[format]){buildCompiledOutputFormat(format);}
return executeCompiledOutputFormat(date,format,localeCode);}
function compareDate(d,find,localeCode,buffer,forceUTC){var p,t,min,max,override,capitalized,accuracy=0,loBuffer=0,hiBuffer=0;p=getExtendedDate(find,localeCode,null,forceUTC);if(buffer>0){loBuffer=hiBuffer=buffer;override=true;}
if(!p.date.isValid())return false;if(p.set&&p.set.specificity){DateUnits.forEach(function(u,i){if(u.name===p.set.specificity){accuracy=u.multiplier(p.date,d-p.date)-1;}});capitalized=simpleCapitalize(p.set.specificity);if(p.set['edge']||p.set['shift']){p.date['beginningOf'+capitalized]();}
if(p.set.specificity==='month'){max=p.date.clone()['endOf'+capitalized]().getTime();}
if(!override&&p.set['sign']&&p.set.specificity!='millisecond'){loBuffer=50;hiBuffer=-50;}}
t=d.getTime();min=p.date.getTime();max=max||(min+accuracy);max=compensateForTimezoneTraversal(d,min,max);return t>=(min-loBuffer)&&t<=(max+hiBuffer);}
function compensateForTimezoneTraversal(d,min,max){var dMin,dMax,minOffset,maxOffset;dMin=new date(min);dMax=new date(max).utc(d.isUTC());if(callDateGet(dMax,'Hours')!==23){minOffset=dMin.getTimezoneOffset();maxOffset=dMax.getTimezoneOffset();if(minOffset!==maxOffset){max+=(maxOffset-minOffset).minutes();}}
return max;}
function updateDate(d,params,reset,advance,prefer){var weekday,specificityIndex;function getParam(key){return isDefined(params[key])?params[key]:params[key+'s'];}
function paramExists(key){return isDefined(getParam(key));}
function uniqueParamExists(key,isDay){return paramExists(key)||(isDay&&paramExists('weekday'));}
function canDisambiguate(){switch(prefer){case-1:return d>getNewDate();case 1:return d<getNewDate();}}
if(isNumber(params)&&advance){params={'milliseconds':params};}else if(isNumber(params)){d.setTime(params);return d;}
if(isDefined(params['date'])){params['day']=params['date'];}
iterateOverDateUnits(function(name,unit,i){var isDay=name==='day';if(uniqueParamExists(name,isDay)){params.specificity=name;specificityIndex=+i;return false;}else if(reset&&name!=='week'&&(!isDay||!paramExists('week'))){callDateSet(d,unit.method,(isDay?1:0));}});DateUnits.forEach(function(u,i){var name=u.name,method=u.method,higherUnit=DateUnits[i-1],value;value=getParam(name)
if(isUndefined(value))return;if(advance){if(name==='week'){value=(params['day']||0)+(value*7);method='Date';}
value=(value*advance)+callDateGet(d,method);}else if(name==='month'&&paramExists('day')){callDateSet(d,'Date',15);}
callDateSet(d,method,value);if(advance&&name==='month'){checkMonthTraversal(d,value);}});if(!advance&&!paramExists('day')&&paramExists('weekday')){var weekday=getParam('weekday'),isAhead,futurePreferred;d.setWeekday(weekday);}
if(canDisambiguate()){iterateOverDateUnits(function(name,unit){var ambiguous=unit.ambiguous||(name==='week'&&paramExists('weekday'));if(ambiguous&&!uniqueParamExists(name,name==='day')){d[unit.addMethod](prefer);return false;}},specificityIndex+1);}
return d;}
function prepareTime(format,loc,iso){var timeSuffixMapping={'h':0,'m':1,'s':2},add;loc=loc||English;return format.replace(/{([a-z])}/g,function(full,token){var separators=[],isHours=token==='h',tokenIsRequired=isHours&&!iso;if(token==='t'){return loc['ampm'].join('|');}else{if(isHours){separators.push(':');}
if(add=loc['timeSuffixes'][timeSuffixMapping[token]]){separators.push(add+'\\s*');}
return separators.length===0?'':'(?:'+separators.join('|')+')'+(tokenIsRequired?'':'?');}});}
function checkMonthTraversal(date,targetMonth){if(targetMonth<0){targetMonth=targetMonth%12+12;}
if(targetMonth%12!=callDateGet(date,'Month')){callDateSet(date,'Date',0);}}
function createDate(args,prefer,forceUTC){var f,localeCode;if(isNumber(args[1])){f=collectDateArguments(args)[0];}else{f=args[0];localeCode=args[1];}
return getExtendedDate(f,localeCode,prefer,forceUTC).date;}
function invalidateDate(d){d.setTime(NaN);}
function buildDateUnits(){DateUnitsReversed=DateUnits.concat().reverse();DateArgumentUnits=DateUnits.concat();DateArgumentUnits.splice(2,1);}
function buildDateMethods(){extendSimilar(date,true,true,DateUnits,function(methods,u,i){var name=u.name,caps=simpleCapitalize(name),multiplier=u.multiplier(),since,until;u.addMethod='add'+caps+'s';function applyErrorMargin(ms){var num=ms/multiplier,fraction=num%1,error=u.error||0.999;if(fraction&&abs(fraction%1)>error){num=round(num);}
return num<0?ceil(num):floor(num);}
since=function(f,localeCode){return applyErrorMargin(this.getTime()-date.create(f,localeCode).getTime());};until=function(f,localeCode){return applyErrorMargin(date.create(f,localeCode).getTime()-this.getTime());};methods[name+'sAgo']=until;methods[name+'sUntil']=until;methods[name+'sSince']=since;methods[name+'sFromNow']=since;methods[u.addMethod]=function(num,reset){var set={};set[name]=num;return this.advance(set,reset);};buildNumberToDateAlias(u,multiplier);if(i<3){['Last','This','Next'].forEach(function(shift){methods['is'+shift+caps]=function(){return compareDate(this,shift+' '+name,'en');};});}
if(i<4){methods['beginningOf'+caps]=function(){var set={};switch(name){case'year':set['year']=callDateGet(this,'FullYear');break;case'month':set['month']=callDateGet(this,'Month');break;case'day':set['day']=callDateGet(this,'Date');break;case'week':set['weekday']=0;break;}
return this.set(set,true);};methods['endOf'+caps]=function(){var set={'hours':23,'minutes':59,'seconds':59,'milliseconds':999};switch(name){case'year':set['month']=11;set['day']=31;break;case'month':set['day']=this.daysInMonth();break;case'week':set['weekday']=6;break;}
return this.set(set,true);};}});}
function buildCoreInputFormats(){English.addFormat('([+-])?(\\d{4,4})[-.]?{full_month}[-.]?(\\d{1,2})?',true,['year_sign','year','month','date'],false,true);English.addFormat('(\\d{1,2})[-.\\/]{full_month}(?:[-.\\/](\\d{2,4}))?',true,['date','month','year'],true);English.addFormat('{full_month}[-.](\\d{4,4})',false,['month','year']);English.addFormat('\\/Date\\((\\d+(?:[+-]\\d{4,4})?)\\)\\/',false,['timestamp'])
English.addFormat(prepareTime(RequiredTime,English),false,TimeFormat)
CoreDateFormats=English.compiledFormats.slice(0,7).reverse();English.compiledFormats=English.compiledFormats.slice(7).concat(CoreDateFormats);}
function buildFormatTokens(){createPaddedToken('f',function(d){return callDateGet(d,'Milliseconds');},true);createPaddedToken('s',function(d){return callDateGet(d,'Seconds');});createPaddedToken('m',function(d){return callDateGet(d,'Minutes');});createPaddedToken('h',function(d){return callDateGet(d,'Hours')%12||12;});createPaddedToken('H',function(d){return callDateGet(d,'Hours');});createPaddedToken('d',function(d){return callDateGet(d,'Date');});createPaddedToken('M',function(d){return callDateGet(d,'Month')+1;});createMeridianTokens();createWeekdayTokens();createMonthTokens();DateFormatTokens['ms']=DateFormatTokens['f'];DateFormatTokens['milliseconds']=DateFormatTokens['f'];DateFormatTokens['seconds']=DateFormatTokens['s'];DateFormatTokens['minutes']=DateFormatTokens['m'];DateFormatTokens['hours']=DateFormatTokens['h'];DateFormatTokens['24hr']=DateFormatTokens['H'];DateFormatTokens['12hr']=DateFormatTokens['h'];DateFormatTokens['date']=DateFormatTokens['d'];DateFormatTokens['day']=DateFormatTokens['d'];DateFormatTokens['year']=DateFormatTokens['yyyy'];}
function buildFormatShortcuts(){extendSimilar(date,true,true,'short,long,full',function(methods,name){methods[name]=function(localeCode){return formatDate(this,name,false,localeCode);}});}
function buildAsianDigits(){KanjiDigits.split('').forEach(function(digit,value){var holder;if(value>9){value=pow(10,value-9);}
AsianDigitMap[digit]=value;});simpleMerge(AsianDigitMap,NumberNormalizeMap);AsianDigitReg=regexp('([期週周])?(['+KanjiDigits+FullWidthDigits+']+)(?!昨)','g');}
function buildRelativeAliases(){var special='today,yesterday,tomorrow,weekday,weekend,future,past'.split(',');var weekdays=English['weekdays'].slice(0,7);var months=English['months'].slice(0,12);extendSimilar(date,true,true,special.concat(weekdays).concat(months),function(methods,name){methods['is'+simpleCapitalize(name)]=function(utc){return this.is(name,0,utc);};});}
function buildUTCAliases(){if(date['utc'])return;date['utc']={'create':function(){return createDate(arguments,0,true);},'past':function(){return createDate(arguments,-1,true);},'future':function(){return createDate(arguments,1,true);}};}
function setDateProperties(){extend(date,false,true,{'RFC1123':'{Dow}, {dd} {Mon} {yyyy} {HH}:{mm}:{ss} {tz}','RFC1036':'{Weekday}, {dd}-{Mon}-{yy} {HH}:{mm}:{ss} {tz}','ISO8601_DATE':'{yyyy}-{MM}-{dd}','ISO8601_DATETIME':'{yyyy}-{MM}-{dd}T{HH}:{mm}:{ss}.{fff}{isotz}'});}
extend(date,false,true,{'create':function(){return createDate(arguments);},'past':function(){return createDate(arguments,-1);},'future':function(){return createDate(arguments,1);},'addLocale':function(localeCode,set){return setLocalization(localeCode,set);},'setLocale':function(localeCode,set){var loc=getLocalization(localeCode,false);CurrentLocalization=loc;if(localeCode&&localeCode!=loc['code']){loc['code']=localeCode;}
return loc;},'getLocale':function(localeCode){return!localeCode?CurrentLocalization:getLocalization(localeCode,false);},'addFormat':function(format,match,localeCode){addDateInputFormat(getLocalization(localeCode),format,match);}});extend(date,true,true,{'set':function(){var args=collectDateArguments(arguments);return updateDate(this,args[0],args[1])},'setWeekday':function(dow){if(isUndefined(dow))return;return callDateSet(this,'Date',callDateGet(this,'Date')+dow-callDateGet(this,'Day'));},'setISOWeek':function(week){var weekday=callDateGet(this,'Day')||7;if(isUndefined(week))return;this.set({'month':0,'date':4});this.set({'weekday':1});if(week>1){this.addWeeks(week-1);}
if(weekday!==1){this.advance({'days':weekday-1});}
return this.getTime();},'getISOWeek':function(){return getWeekNumber(this);},'beginningOfISOWeek':function(){var day=this.getDay();if(day===0){day=-6;}else if(day!==1){day=1;}
this.setWeekday(day);return this.reset();},'endOfISOWeek':function(){if(this.getDay()!==0){this.setWeekday(7);}
return this.endOfDay()},'getUTCOffset':function(iso){var offset=this._utc?0:this.getTimezoneOffset();var colon=iso===true?':':'';if(!offset&&iso)return'Z';return padNumber(floor(-offset/60),2,true)+colon+padNumber(abs(offset%60),2);},'utc':function(set){defineProperty(this,'_utc',set===true||arguments.length===0);return this;},'isUTC':function(){return!!this._utc||this.getTimezoneOffset()===0;},'advance':function(){var args=collectDateArguments(arguments,true);return updateDate(this,args[0],args[1],1);},'rewind':function(){var args=collectDateArguments(arguments,true);return updateDate(this,args[0],args[1],-1);},'isValid':function(){return!isNaN(this.getTime());},'isAfter':function(d,margin,utc){return this.getTime()>date.create(d).getTime()-(margin||0);},'isBefore':function(d,margin){return this.getTime()<date.create(d).getTime()+(margin||0);},'isBetween':function(d1,d2,margin){var t=this.getTime();var t1=date.create(d1).getTime();var t2=date.create(d2).getTime();var lo=min(t1,t2);var hi=max(t1,t2);margin=margin||0;return(lo-margin<t)&&(hi+margin>t);},'isLeapYear':function(){var year=callDateGet(this,'FullYear');return(year%4===0&&year%100!==0)||(year%400===0);},'daysInMonth':function(){return 32-callDateGet(new date(callDateGet(this,'FullYear'),callDateGet(this,'Month'),32),'Date');},'format':function(f,localeCode){return formatDate(this,f,false,localeCode);},'relative':function(fn,localeCode){if(isString(fn)){localeCode=fn;fn=null;}
return formatDate(this,fn,true,localeCode);},'is':function(d,margin,utc){var tmp,comp;if(!this.isValid())return;if(isString(d)){d=d.trim().toLowerCase();comp=this.clone().utc(utc);switch(true){case d==='future':return this.getTime()>getNewDate().getTime();case d==='past':return this.getTime()<getNewDate().getTime();case d==='weekday':return callDateGet(comp,'Day')>0&&callDateGet(comp,'Day')<6;case d==='weekend':return callDateGet(comp,'Day')===0||callDateGet(comp,'Day')===6;case(tmp=English['weekdays'].indexOf(d)%7)>-1:return callDateGet(comp,'Day')===tmp;case(tmp=English['months'].indexOf(d)%12)>-1:return callDateGet(comp,'Month')===tmp;}}
return compareDate(this,d,null,margin,utc);},'reset':function(unit){var params={},recognized;unit=unit||'hours';if(unit==='date')unit='days';recognized=DateUnits.some(function(u){return unit===u.name||unit===u.name+'s';});params[unit]=unit.match(/^days?/)?1:0;return recognized?this.set(params,true):this;},'clone':function(){var d=new date(this.getTime());d.utc(!!this._utc);return d;}});extend(date,true,true,{'iso':function(){return this.toISOString();},'getWeekday':date.prototype.getDay,'getUTCWeekday':date.prototype.getUTCDay});function buildNumberToDateAlias(u,multiplier){var name=u.name,methods={};function base(){return round(this*multiplier);}
function after(){return createDate(arguments)[u.addMethod](this);}
function before(){return createDate(arguments)[u.addMethod](-this);}
methods[name]=base;methods[name+'s']=base;methods[name+'Before']=before;methods[name+'sBefore']=before;methods[name+'Ago']=before;methods[name+'sAgo']=before;methods[name+'After']=after;methods[name+'sAfter']=after;methods[name+'FromNow']=after;methods[name+'sFromNow']=after;number.extend(methods);}
extend(number,true,true,{'duration':function(localeCode){return getLocalization(localeCode).getDuration(this);}});English=CurrentLocalization=date.addLocale('en',{'plural':true,'timeMarker':'at','ampm':'am,pm','months':'January,February,March,April,May,June,July,August,September,October,November,December','weekdays':'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday','units':'millisecond:|s,second:|s,minute:|s,hour:|s,day:|s,week:|s,month:|s,year:|s','numbers':'one,two,three,four,five,six,seven,eight,nine,ten','articles':'a,an,the','tokens':'the,st|nd|rd|th,of','short':'{Month} {d}, {yyyy}','long':'{Month} {d}, {yyyy} {h}:{mm}{tt}','full':'{Weekday} {Month} {d}, {yyyy} {h}:{mm}:{ss}{tt}','past':'{num} {unit} {sign}','future':'{num} {unit} {sign}','duration':'{num} {unit}','modifiers':[{'name':'sign','src':'ago|before','value':-1},{'name':'sign','src':'from now|after|from|in|later','value':1},{'name':'edge','src':'last day','value':-2},{'name':'edge','src':'end','value':-1},{'name':'edge','src':'first day|beginning','value':1},{'name':'shift','src':'last','value':-1},{'name':'shift','src':'the|this','value':0},{'name':'shift','src':'next','value':1}],'dateParse':['{month} {year}','{shift} {unit=5-7}','{0?} {date}{1}','{0?} {edge} of {shift?} {unit=4-7?}{month?}{year?}'],'timeParse':['{num} {unit} {sign}','{sign} {num} {unit}','{0} {num}{1} {day} of {month} {year?}','{weekday?} {month} {date}{1?} {year?}','{date} {month} {year}','{date} {month}','{shift} {weekday}','{shift} week {weekday}','{weekday} {2?} {shift} week','{num} {unit=4-5} {sign} {day}','{0?} {date}{1} of {month}','{0?}{month?} {date?}{1?} of {shift} {unit=6-7}']});buildDateUnits();buildDateMethods();buildCoreInputFormats();buildFormatTokens();buildFormatShortcuts();buildAsianDigits();buildRelativeAliases();buildUTCAliases();setDateProperties();})();