(function($) {
	function CaretPositionCtrl(element) {
		var input = element.jquery ? element.get(0) : element;
		return {
			getPosition: function() {
				var begin = 0,
					end = 0;
				if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000), end = begin + range.text.length;
				} else if (input.setSelectionRange) {
					begin = input.selectionStart;
					end = input.selectionEnd;
				}
				return {
					begin: begin,
					end: end
				};
			},
			setPosition: function(position) {
				if (!$(input).is(':focus')) return;
				if (input.createTextRange) {
					var range = input.createTextRange();
					range.collapse(true);
					range.moveEnd('character', position);
					range.moveStart('character', position);
					range.select();
				} else if (input.setSelectionRange) {
					input.selectionStart = position;
					input.selectionEnd = position;
				}
			}
		};
	};

	function InputCtrl(element, updateViewValueCallback) {
		var maskList = (new PhoneMaskSingleton).sortedMask || [],
			nonPhonesCharRegexp = new RegExp('\\D'),
			phoneDigitsRegexp = new RegExp('[0-9\\+]'),
			placeholder = '#',
			updateCallback = updateViewValueCallback || function(value, mask, valid) {
				element.val(value);
			},
			caretCtrl = new CaretPositionCtrl(element),
			fallbackValueLength = 0,
			MAX_PHONE_LENGTH = 15;
		var replaced = false,
			deleteKey = false;
		var applyMask = function(mask, phone, allowOverflow) {
			var result = "",
				mi = 0,
				pi = 0,
				ml = mask.length,
				pl = phone.length;
			while (pi < pl && mi < ml) {
				var phoneChar = phone.charAt(pi),
					maskChar = mask.charAt(mi);
				if (maskChar == placeholder || maskChar == phoneChar) {
					pi++;
					mi++;
					result += phoneChar;
					continue;
				}
				if (nonPhonesCharRegexp.test(maskChar)) {
					mi++;
					result += maskChar;
					continue;
				}
				return false;
			}
			return !allowOverflow && mi == ml && pi < pl ? undefined : result;
		};
		var processKey = function(value, caret, charCode) {
			var _char = String.fromCharCode(charCode),
				valueArray = value.split(''),
				delta = caret.end - caret.begin;
			if (delta > 0) {
				valueArray.splice(caret.begin, delta, _char);
			} else {
				valueArray.splice(caret.begin, 0, _char);
			}
			return valueArray.join('');
		};
		var processValue = function(value, allowOverflow) {
			var i = 0,
				len = maskList.length,
				phone, result, mask;
			replaced = (parseInt(value[0], 10) === 8);
			phone = phoneFilter(value);
			for (; i < len; i++) {
				mask = maskList[i].mask;
				result = applyMask(mask, phone, allowOverflow);
				if (result) {
					replaced && (result = result.replace(/^(\+7)+/, '8'));
					updateCallback.apply(element, [result, maskList[i], true]);
					return result;
				}
				if (result === undefined && !deleteKey) return false;
			}
			value = replaced ? phone.replace(/^(\+7)+/, '8') : phone;
			updateCallback.apply(element, [value, '', false]);
			return value;
		};
		var phoneFilter = function(value) {
			var firstChar;
			value = replaced ? value.replace(/^(8)+/, '+7') : value;
			value = value.replace(/^(\+8)+/, '+7');
			firstChar = value.charAt(0);
			value = value.replace(/\D/g, '');
			value = value.slice(0, MAX_PHONE_LENGTH);
			if (firstChar == '+') {
				value = '+' + value;
			}
			return value;
		};
		var processCaretPosition = function(value, caretPosition) {
			return getNextCaretPosition(value, caretPosition);
		};
		var getNextCaretPosition = function(value, caretPosition) {
			while (++caretPosition <= value.length) {
				if (phoneDigitsRegexp.test(value.charAt(caretPosition - 1))) return caretPosition;
			}
			return --caretPosition;
		};
		var getPrevCaretPosition = function(value, caretPosition) {
			while (--caretPosition >= 0) {
				if (phoneDigitsRegexp.test(value.charAt(caretPosition - 1))) return caretPosition;
			}
			return ++caretPosition;
		};
		element.on('keydown', function(event) {
			deleteKey = false;
			var KEY_BACKSPACE = 8,
				KEY_DELETE = 46,
				key = event.which;
			if (!event.ctrlKey && !event.altKey && ((event.keyCode > 57 && event.keyCode < 91) || (event.keyCode == 106) || (event.keyCode > 107 && event.keyCode < 112) || (event.keyCode > 157))) {
				event.preventDefault();
			}
			if (!event.ctrlKey && !event.altKey && !event.shiftKey && (key == KEY_BACKSPACE || key == KEY_DELETE)) {
				event.preventDefault();
				deleteKey = true;
				var currentValue = this.value,
					carPos = caretCtrl.getPosition(),
					pos = carPos.begin;
				if (carPos.end == carPos.begin) {
					if (key === KEY_BACKSPACE) {
						pos = carPos.begin = getPrevCaretPosition(currentValue, carPos.begin);
					} else {
						carPos.end = getNextCaretPosition(currentValue, carPos.end);
					}
				}
				processValue(processKey(currentValue, carPos, ''));
				caretCtrl.setPosition(pos);
			}
		});
		element.on('keypress', function(event) {
			if (event.ctrlKey || event.altKey || (!event.charCode && !$.browser.msie)) {
				return;
			}
			deleteKey = false;
			event.preventDefault();
			fallbackValueLength = false;
			var currentValue = this.value,
				currentCaret = caretCtrl.getPosition(),
				plus = 0;
			if (currentCaret.begin != currentCaret.end) {
				deleteKey = true;
			}
			if (currentValue.replace(/\D/g, '').length >= MAX_PHONE_LENGTH && !deleteKey) {
				return;
			}
			var updatedValue = processValue(processKey(currentValue, currentCaret, event.which));
			if ((updatedValue.length - currentValue.length) > 1) {
				plus = 1;
			}
			if (updatedValue) {
				caretCtrl.setPosition(getNextCaretPosition(updatedValue, currentCaret.begin + plus));
			}
		});
		element.on('input change', function(event) {
			event.preventDefault();
			var currentCaret = caretCtrl.getPosition(),
				currentValue = this.value,
				updatedValue = processValue(this.value);
			if (updatedValue) {
				if (fallbackValueLength !== false && fallbackValueLength <= currentValue.length) {
					caretCtrl.setPosition(processCaretPosition(updatedValue, --currentCaret.begin) + 1);
				} else {
					caretCtrl.setPosition(processCaretPosition(updatedValue, currentCaret.begin));
				}
				fallbackValueLength = updatedValue.length;
			}
		});
		element.on('paste', function(event) {
			setTimeout(function() {
				var value = processValue(element.get(0).value);
				fallbackValueLength = value.length;
			}, 0);
		});
		setTimeout(function() {
			if (element.get(0).value) {
				var value = processValue(element.get(0).value);
				fallbackValueLength = value.length;
			}
		}, 0);
	}


	$.fn.mmask = function() {
			console.log("SDg");
		}

	


	angular.module('angie').directive('sjMaskedPhone', function() {
		return {
			require: ['ngModel'],
			link: function(scope, element, attrs, ctrls) {
				var modelCtrl = ctrls[0],
					digest = _.debounce(function() {}, 200);
				new InputCtrl(element, function(value, mask, valid) {
					scope.phone.valid = valid;
					if (mask && attrs.sjMaskedNotmobileModel) {

						console.log(attrs.sjMaskedNotmobileModel + '=' + (mask.type && mask.type == 'mobile' ? 'false' : 'true'));
						console.log(value);
						console.log(valid);

						scope.$eval(attrs.sjMaskedNotmobileModel + '=' + (mask.type && mask.type == 'mobile' ? 'false' : 'true'));
					}
					modelCtrl.$setViewValue(value);
					modelCtrl.$render();
					scope.$digest();
				});
			}
		};
	});



	// $.fn.sjMaskedPhone = function(options) {
	// 	this.each(function() {
	// 		new InputCtrl($(this));
	// };

	
	

})($);