'use strict';function TownList()
{var self=this;var $typeOfSelect='single';var $selectable_regions=false;var $popupCallerId=$('#id-for-town-popup');var $getSelectedTowns;var $actionOnSelectButtonClick;var $selectedTowns;var selection={};var $container;var geoEntitiesFilter={};if(typeof TownList.prototype.numInstance=='undefined')TownList.prototype.numInstance=0;TownList.prototype.numInstance++;var $numInstance=TownList.prototype.numInstance;var AllTowns;var popup_create=false;var clickShowPopUpHandler=function(e)
{if(!popup_create)
{self.createPopUp();}
else
{self.showModal();}
e.preventDefault();};this.buildTree=function(input_tree,levelLimit)
{var mainDiv,branch_toggle,childDiv,chkbox;var plus='openclose_cat2_plusminus_only openclose_cat2_plus';var minus='openclose_cat2_plusminus_only openclose_cat2_minus';var output=document.createElement('span');(levelLimit)?levelLimit=levelLimit:levelLimit=0;$.each(input_tree,function(key,val)
{if(val['children']!='0')
{plus='openclose_cat_plusminus_only openclose_cat_plus';minus='openclose_cat_plusminus_only openclose_cat_minus';mainDiv=document.createElement('div');mainDiv.id='parent'+$numInstance+val['id'];if($typeOfSelect=='multi')
{if($selectable_regions){branch_toggle=document.createElement('div');if(levelLimit!=0&&val['children']!='1')
{branch_toggle.className=minus;}
else
{branch_toggle.className=plus;};branch_toggle.appendChild(document.createTextNode(''));mainDiv.appendChild(branch_toggle);chkbox=document.createElement('div');chkbox.className='townslist_checkbox';chkbox.innerHTML='<input '+
(selection[val['id']]==true?'checked ':'')+'name="townselect_checkbox" type="checkbox" value="'+
val['value']+'" id="'+val['id']+'-'+$numInstance+'"'+'data-type="'+val['type']+'">';mainDiv.appendChild(chkbox);$(chkbox).children('#'+val['id']+'-'+$numInstance).unbind();$(chkbox).children('#'+val['id']+'-'+$numInstance).click(function(){if(this.checked)
{self.addToSelection(val['id']);self.selectAllChildren(val['id']);self.trySelectParents(val['id']);}
else
{self.removeFromSelection(val['id']);self.deselectAllChildren(val['id']);self.deselectParents(val['id']);}});chkbox=document.createElement('div');chkbox.className='townslist_item';chkbox.innerHTML='<label for="'+val['id']+'-'+$numInstance+'">'+val['tname']+'</label>';mainDiv.appendChild(chkbox);}else{plus='openclose_cat2_plusminus_only openclose_cat2_plus';minus='openclose_cat2_plusminus_only openclose_cat2_minus';branch_toggle=document.createElement('div');if(levelLimit!=0&&val['children']!=1)
{branch_toggle.className=minus;}
else
{branch_toggle.className=plus;};branch_toggle.appendChild(document.createTextNode(''));branch_toggle.innerHTML=val['tname'];mainDiv.appendChild(branch_toggle);}}
else
{if($selectable_regions&&val['parent']=='c1')
{mainDiv.className='townslist_radio';branch_toggle=document.createElement('div');if(levelLimit!=0&&val['children']!=1)
{branch_toggle.className=minus;}
else
{branch_toggle.className=plus;};branch_toggle.appendChild(document.createTextNode(''));var radio=null;var label=null;try{radio=document.createElement('<input name="townselect_radio-'+$numInstance+'">');}
catch(ex){radio=document.createElement('input');radio.setAttribute('name','townselect_radio-'+$numInstance);}
radio.setAttribute('data-type',val['type']);radio.setAttribute('type','radio');radio.setAttribute('value',val['value']);radio.setAttribute('domain',val['domain']);radio.setAttribute('id',val['id']+'-'+$numInstance);if(selection[val['id']])
{radio.setAttribute('checked',true);}
mainDiv.appendChild(branch_toggle);chkbox=document.createElement('div');chkbox.className='townslist_checkbox';chkbox.appendChild(radio);mainDiv.appendChild(chkbox);$(chkbox).children('#'+val['id']+'-'+$numInstance).unbind();$(chkbox).children('#'+val['id']+'-'+$numInstance).click(function(){if(this.checked)
{$selectedTowns[0]=val['id'];self.clearSelection();selection[val['id']]=true;}});label=document.createElement('label');label.htmlFor=val['id']+'-'+$numInstance;label.appendChild($(document.createElement('span')).html(val['tname']).get(0));chkbox=document.createElement('div');chkbox.className='townslist_item';chkbox.appendChild(label);mainDiv.appendChild(chkbox);}
else
{plus='openclose_cat2_plusminus_only openclose_cat2_plus';minus='openclose_cat2_plusminus_only openclose_cat2_minus';branch_toggle=document.createElement('div');if(levelLimit!=0&&val['children']!=1)
{branch_toggle.className=minus;}
else
{branch_toggle.className=plus;};branch_toggle.appendChild(document.createTextNode(''));branch_toggle.innerHTML=val['tname'];mainDiv.appendChild(branch_toggle);}};output.appendChild(mainDiv);$(branch_toggle).click(function()
{var sublevel;if(this.className==plus)
{var original_node=self.nodeById(val['id']),subnodes_list=original_node['children'],filter_text=$('#town_search'+$numInstance).val();if(filter_text){subnodes_list=self.applySearchTownFilter(subnodes_list,self.fixLanguage(filter_text).toLowerCase(),$typeOfSelect!='multi');}
sublevel=document.createElement('div');sublevel.className='townslist_sublevel';sublevel.id='child'+$numInstance+val['id'];sublevel.appendChild(self.buildTree(subnodes_list));$(sublevel).insertAfter('#parent'+$numInstance+val['id']);this.className=minus;self.markSelectedTowns();}
else
{sublevel=document.getElementById('child'+$numInstance+val['id']);output.removeChild(sublevel);this.className=plus;}});if(levelLimit!=0&&val['children']!='1')
{childDiv=document.createElement('div');childDiv.className='townslist_sublevel';childDiv.id='child'+$numInstance+val['id'];childDiv.appendChild(self.buildTree(val['children'],levelLimit-1));output.appendChild(childDiv);}}
else
{mainDiv=document.createElement('div');if($typeOfSelect=='multi')
{mainDiv.className='townslist_checkb';chkbox=document.createElement('div');chkbox.className='townslist_checkbox_sublevel';chkbox.innerHTML='<input '+
(selection[val['id']]==true?'checked ':'')+'name="townselect_checkbox" type="checkbox" value="'+
val['value']+'" id="'+val['id']+'-'+$numInstance+'"'+'data-type="'+val['type']+'">';mainDiv.appendChild(chkbox);$(chkbox).children('#'+val['id']+'-'+$numInstance).unbind();$(chkbox).children('#'+val['id']+'-'+$numInstance).click(function(){if(this.checked)
{self.addToSelection(val['id']);self.trySelectParents(val['id']);}
else
{self.removeFromSelection(val['id']);self.deselectParents(val['id']);}});chkbox=document.createElement('div');chkbox.className='townslist_item_sublevel';chkbox.innerHTML='<label for="'+val['id']+'-'+$numInstance+'">'+val['tname']+'</label>';mainDiv.appendChild(chkbox);}
else
{mainDiv.className='townslist_radio';var radio=null;var label=null;try{radio=document.createElement('<input name="townselect_radio-'+$numInstance+'">');}
catch(ex){radio=document.createElement('input');radio.setAttribute('name','townselect_radio-'+$numInstance);}
radio.setAttribute('type','radio');radio.setAttribute('data-type',val['type']);radio.setAttribute('value',val['value']);radio.setAttribute('domain',val['domain']);radio.setAttribute('id',val['id']+'-'+$numInstance);if(selection[val['id']])
{radio.setAttribute('checked',true);}
label=document.createElement('label');label.htmlFor=val['id']+'-'+$numInstance;label.appendChild($(document.createElement('span')).html(val['tname']).get(0));mainDiv.appendChild(radio);mainDiv.appendChild(label);$(mainDiv).children('#'+val['id']+'-'+$numInstance).unbind();$(mainDiv).children('#'+val['id']+'-'+$numInstance).click(function(){if(this.checked)
{$selectedTowns[0]=val['id'];self.clearSelection();selection[val['id']]=true;}});}
mainDiv.className+=" t_child";output.appendChild(mainDiv);}});return output;}
this.init=function(properties)
{$typeOfSelect=properties.typeOfSelect||$typeOfSelect;$selectable_regions=properties.selectable_regions||$selectable_regions;if(properties.popupCallerId.jquery){$popupCallerId=properties.popupCallerId;}else{$popupCallerId=properties.popupCallerId?$('#'+properties.popupCallerId):$popupCallerId;}
$actionOnSelectButtonClick=properties.clickSelectButton;self.clearSelection();$getSelectedTowns=function(){var arr=properties.getSelectedTowns(),key;if({}.toString.call(arr)!='[object Array]'){arr=[];}
if(!arr.indexOf)arr.indexOf=self.indexOfArray;self.clearSelection();for(key in arr){selection[arr[key]]=true;}
return arr;};if(typeof properties.geoEntitiesFilter=='object'){geoEntitiesFilter=properties.geoEntitiesFilter;}
self.createPopUpContainer();self.bindPopUpLink();self.Search();};this.createPopUpContainer=function()
{$container=$('<div id="townlist_popup_container'+$numInstance+'" style="display: none;">').appendTo('body');$container.append(' <div class="uprise_block" style="width: 500px">'+'<div class="uprise_block_inner">'+'<a class="window_popup_close innerclose simplemodal-close" title="Закрыть" href="javascript://"></a>'+'<div class="townselect_popup_content" id="tl_container'+$numInstance+'">'+'<div class="space22"></div>'+'<div class="bigger bold">Выбор города</div>'+'<div class="columns_input window_town_search">'+'<input name="" id="town_search'+$numInstance+'" type="text" maxlength="100" value=""/></div>'+'<div class="space15" id="postlist'+$numInstance+'"></div>'+'</div>'+'<div class="uprise_block_footer uprise_block_footer_padding">'+'<table cellspacing="0">'+'<tr>'+'<td>'+'<table cellspacing="0" cellpadding="0" border="0" class="fix-active fix-hover button">'+'<tr>'+'<td>'+'<span class="btn btn_medium pie">'+'<input name="" type="button" value="Выбрать" id="tl_select_button'+$numInstance+'">'+'</span>'+'</td>'+'</tr>'+'</table>'+'</td>'+'<td><div class="townselect_footer_cancel simplemodal-close"><a class="dashedlink_black">Отменить</a></div></td>'+'</tr>'+'</table>'+'</div>'+'</div>'+'</div>');};this.bindPopUpLink=function()
{$popupCallerId.click(clickShowPopUpHandler);};this.createPopUp=function()
{$.getJSON('/js/request/townlist_popup.php',geoEntitiesFilter,function(data)
{AllTowns=data;popup_create=true;self.showModal();});};this.showModal=function()
{var townContainer;$selectedTowns=$getSelectedTowns($('#townlist_popup_container'+$numInstance));$('#tl_select_button'+$numInstance).unbind();$('#tl_select_button'+$numInstance).click(function(){var townsTree=self.getSelectedTownsTree(AllTowns);townsTree.getSelectedNodesAsArray=self.getSelectedNodesAsArray;$actionOnSelectButtonClick(townsTree);});$('#townlist_sheet'+$numInstance).remove();townContainer=document.createElement('div');townContainer.className='townselect_popup_overflow';townContainer.id='townlist_sheet'+$numInstance;townContainer.setAttribute('data-type',$typeOfSelect);townContainer.appendChild(self.buildTree(self.applySelectedTownFilter(AllTowns),-1));$(townContainer).insertBefore('#postlist'+$numInstance);$('#townlist_popup_container'+$numInstance).modal({containerId:'townlist-popup-simplemodal-container',containerCss:{width:500,height:535,visibility:'hidden'},onOpen:function(dialog){dialog.overlay.show();dialog.container.css('visibility','visible');dialog.container.show();dialog.data.show();if(window.PIE){dialog.data.find("span.pie").each(function(){window.PIE.attach(this);});}},onClose:function(dialog){if(window.PIE){dialog.data.find("span.pie").each(function(){window.PIE.detach(this);});}
$.modal.close();}});if($selectedTowns.length>0)
{self.markSelectedTowns();if($typeOfSelect!='multi')
{var $selectedElement=$(document.getElementsByName('townselect_radio-'+$numInstance)).filter(":checked");if($selectedElement[0]!=undefined){$('#townlist_sheet'+$numInstance).animate({scrollTop:$(document.getElementsByName('townselect_radio-'+$numInstance)).filter(":checked").offset().top-400-$(window).scrollTop()},'fast');}}
else
{$.each($selectedTowns,function(key,node){self.selectAllChildren(node);});var $selectedElement=$('#townlist_sheet'+$numInstance).find(':checkbox:checked').first();if($selectedElement[0]!=undefined){$('#townlist_sheet'+$numInstance).animate({scrollTop:$selectedElement.offset().top-400-$(window).scrollTop()},'fast');}}}};this.Search=function()
{var search;var FilteredTowns;$(document).ready(function()
{$('#town_search'+$numInstance).keyup(function(e)
{$('#townlist_sheet'+$numInstance).empty();if($('#town_search'+$numInstance).val().length>=1)
{search=e.target.value;FilteredTowns=self.applySearchTownFilter(AllTowns,self.fixLanguage(search).toLowerCase(),$typeOfSelect!='multi');document.getElementById('townlist_sheet'+$numInstance).appendChild(self.buildTree(FilteredTowns,-1));}
else
{document.getElementById('townlist_sheet'+$numInstance).appendChild(self.buildTree(AllTowns));}});});}
this.matchAndHighlightTownName=function(srch,tname)
{if(tname.toLowerCase().indexOf(srch)!=-1)
{var replaced=tname.replace(new RegExp("(^|[- ])("+srch+")",'gi'),'$1<span class="bgfound">$2</span>');return replaced.length>tname.length?replaced:false;}
return false;}
this.applySearchTownFilter=function(towns,srch,searchOnlyLeafs)
{var found=false;var i=0;var is_leaf=false;$.each(towns,function(key,node)
{var match={'tname':false,'children':false};is_leaf=(node['children']==0);if((is_leaf||!searchOnlyLeafs||($selectable_regions&&node.parent=='c1')))
match['tname']=self.matchAndHighlightTownName(srch,node['tname']);if(!is_leaf)
match['children']=self.applySearchTownFilter(node['children'],srch,searchOnlyLeafs);if(match['tname']||match['children'])
{if(!found)found=new Object;found[i]=match;for(var prop in node)
{if(match[prop]==undefined||!match[prop])
found[i][prop]=node[prop];}}
i=i+1;});return found;}
this.matchSelectedTown=function(town)
{return selection[town['id']]==true;}
this.applySelectedTownFilter=function(towns,level)
{var at_least_one_found=false;var iterated=new Object;var i=0;if(level==undefined)level=0;$.each(towns,function(key,node)
{iterated[i]=new Object;for(var prop in node)
{if(prop!='children')
{iterated[i][prop]=node[prop];}}
iterated[i]['children']='0';if(node['children']!=0)
{iterated[i]['children']='1';var children=self.applySelectedTownFilter(node['children'],level+1);if(children)
{iterated[i]['children']=children;at_least_one_found=true;}}
if(0==level||self.matchSelectedTown(node))
{at_least_one_found=true;}
i=i+1;});return at_least_one_found?iterated:false;}
this.fixLanguage=function(str)
{var layoutEn=new Array("`","q","w","e","r","t","y","u","i","o","p","\\[","\\]","a","s","d","f","g","h","j","k","l",";","'","z","x","c","v","b","n","m","\,","\\.","\\/","~","Q","W","E","R","T","Y","U","I","O","P","\\{","\\}","A","S","D","F","G","H","J","K","L",":","\"","Z","X","C","V","B","N","M","<",">","\\?","\\|","@","#","\\$","\\^","&");var layoutRu=new Array("ё","й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д","ж","э","я","ч","с","м","и","т","ь","б","ю",".","Ё","Й","Ц","У","К","Е","Н","Г","Ш","Щ","З","Х","Ъ","Ф","Ы","В","А","П","Р","О","Л","Д","Ж","Э","Я","Ч","С","М","И","Т","Ь","Б","Ю","\,","/","\"","№",";",":","?");var re="";for(var i=0;i<layoutEn.length;i++)
{re=new RegExp(layoutEn[i],"g");str=str.replace(re,layoutRu[i]);}
return str;}
this.getSelectedTownsTree=function(Towns)
{var ResultTree=new Array();var i=0;$.each(Towns,function(key,node)
{var
foundChildren=false,isSelected=false;if(node['children']!='0')
{foundChildren=self.getSelectedTownsTree(node['children']);}
isSelected=self.matchSelectedTown(node);if(isSelected||foundChildren.length>0)
{i=ResultTree.length;ResultTree[i]=new Object();for(var prop in node)
{if(undefined==ResultTree[i][prop])
ResultTree[i][prop]=node[prop];}
ResultTree[i]['selected']=isSelected?true:false;ResultTree[i]['children']=foundChildren.length>0?foundChildren:'0';}});return ResultTree;}
this.nodeById=function(id,SubTree)
{var found=false;if(SubTree==undefined){SubTree=AllTowns;}
for(var key=0;key<SubTree.length;key++)
{if(SubTree[key]['id']==id)
{return SubTree[key];}
if(SubTree[key]['children']!='0')
{if(found=self.nodeById(id,SubTree[key]['children']))return found;}}
return false;}
this.selectAllChildren=function(id)
{var node=self.nodeById(id),key;if(node.children!='0')
{for(key=0;key<node.children_ids.length;key++)
{self.addToSelection(node.children_ids[key]);}}}
this.deselectAllChildren=function(id)
{var node=self.nodeById(id),key;if(node.children!='0')
{for(key=0;key<node.children_ids.length;key++)
{self.removeFromSelection(node.children_ids[key]);}}}
this.trySelectParents=function(id)
{var node=self.nodeById(id),parent,child,allChildrenSelected=true;if(node.parent!='0')
{parent=self.nodeById(node.parent);$.each(parent.children,function(key,child){if(selection[child.id]!=true){return allChildrenSelected=false;}});if(allChildrenSelected){self.addToSelection(parent.id);self.trySelectParents(parent.id);}}}
this.deselectParents=function(id)
{var node=self.nodeById(id),parent;if(node.parent!='0')
{parent=self.nodeById(node.parent);self.removeFromSelection(parent.id);self.deselectParents(parent.id);}}
this.markSelectedTowns=function()
{var chbox;for(var i=0;i<$selectedTowns.length;i++)
{chbox=document.getElementById($selectedTowns[i]+'-'+$numInstance);if(chbox!=undefined){chbox.checked=true;}}}
this.getSelectedNodesAsArray=function(Tree)
{var town=new Array();var self=this;if(typeof Tree=='undefined')
{Tree=this;}
$.each(Tree,function(k,node)
{if(node['selected'])
{town[town.length]=node;}
if(node['children']!='0')
{var found_town;if((found_town=self.getSelectedNodesAsArray(node['children'])).length>0)
{town=town.concat(found_town);}}});return town;}
this.indexOfArray=function(val)
{for(var i=0;i<this.length;i++)
{if(this[i]==val)
{return i;}}
return-1;}
this.addToSelection=function(id){var chbox;if(selection[id]!=true){selection[id]=true;$selectedTowns[$selectedTowns.length]=id;chbox=document.getElementById(id+'-'+$numInstance);if(chbox!=undefined){chbox.checked=true;}}}
this.removeFromSelection=function(id){var position,chbox;if(selection[id]==true){selection[id]=undefined;position=$selectedTowns.indexOf(id);$selectedTowns.splice(position,1);chbox=document.getElementById(id+'-'+$numInstance);if(chbox!=undefined){chbox.checked=false;}}}
this.clearSelection=function(){selection={};}}
function multiSelect(selector,selected_town)
{this.typeOfSelect='multi';this.popupCallerId=selector||'select-town-multi-popup';var town_selector=selected_town||'selected_town';this.selected_town=$('#'+town_selector);this.selectable_regions=true;var self=this;this.getSelectedTowns=function()
{var selected_towns=[];var pick=function(i,town)
{selected_towns[selected_towns.length]=town['id'];}
if(self.selected_town.attr('id')=='selected_town_move'){$('[name="m_c[]"]',self.selected_town).each(pick);$('[name="m_o[]"]',self.selected_town).each(pick);$('[name="m_t[]"]',self.selected_town).each(pick);}
else{$('[name="c[]"]',self.selected_town).each(pick);$('[name="o[]"]',self.selected_town).each(pick);$('[name="t[]"]',self.selected_town).each(pick);}
return selected_towns;};this.clickSelectButton=function(Towns)
{var towns=self.getSelectedTownsList(Towns);var selected_town=self.selected_town;selected_town.empty();var use_short_name=selected_town.hasClass('use-short-name');var geo_prefix='',geo_class='',town_name='';for(var i=0;i<towns.length;i++)
{geo_prefix=towns[i]['id'].substring(0,1);switch(geo_prefix)
{case'c':geo_class='country';break;case'o':geo_class='oblast';break;case't':geo_class='town';break;}
town_name=(use_short_name)?trimString(towns[i]['tname'],20):towns[i]['tname'];if(selected_town.attr('id')=='selected_town_move'){geo_prefix='m_'+geo_prefix;}
self.selected_town.append('<span class="tl_block_item '+towns[i]['id']+'" title="'+towns[i]['tname']+'"><span class="tl_this_item"><span class="region-name">'+town_name+'</span>'+'<input class="'+geo_class+' '+towns[i]['path']+'" id="'+towns[i]['id']+'" type="hidden" name="'+geo_prefix+'[]" value="'+towns[i]['value']+'"></span>'+'<a class="del_textitem" title="удалить"'+' style="cursor: pointer;">&nbsp;</a></span> ');}
if(towns.length>0)
{$('.clear_town_list',self.selected_town).remove()
self.selected_town.append('<span class="tl_block_item"><a class="clear_town_list" class="dashedlink">очистить</a></span>').css({"display":"block"});}
else
{self.selected_town.attr('style','display: none;');$('.clear_town_list',self.selected_town).attr('style','display: none;');}
$.modal.close();if(selected_town.attr('id')=='selected_town'){self.dispMetro();}
$('#town_block').trigger('selected-towns-changed');};this.getSelectedTownsList=function(Towns,path)
{var List=new Array();if(undefined==path)path='';$.each(Towns,function(k,node)
{if(node['selected'])
{node['path']=path;List.push(node);}
if(node['children']!=0&&!node['selected'])
{List=List.concat(self.getSelectedTownsList(node['children'],path!=''?(path+' '+node['id']):node['id']));}});return List;};this.dispMetro=function(){if(typeof window.towns_with_metro=='undefined'){return;}
var $inp=$('input',$('#selected_town'));if($inp.length==1&&$inp.hasClass('town')){sendMetroRequest($inp.val());}
else{sendMetroRequest(0);}};}