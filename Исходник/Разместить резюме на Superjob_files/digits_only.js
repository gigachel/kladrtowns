
function getChar(event){if(event.which==null){if(event.keyCode<32)return null;return String.fromCharCode(event.keyCode)}
if(event.which!=0&&event.charCode!=0){if(event.which<32)return null;return String.fromCharCode(event.which);}
return null;}
function digitsOnly(evt)
{var chr=getChar(evt);if(chr==null)
return true;var digit_chars='0123456789';if(digit_chars.indexOf(chr)==-1){return false;}
return true;}
$(document).ready(function(){$("input.digitsOnly").bind('keypress',function(event){if(!digitsOnly(event)){return false;}});});